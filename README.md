# Mastermind Numbers Solver

This little script that I made in one afternoon is an AI that will find a number of LENGTH digits (found at the top of main.py)

All you have to do is tell it how many digits from it's guess are in common and in the right position with your number, and how many are in common but misplaced.

> Your number needs to not have repeated digits, otherwise it will not work

## Installation & Usage

You need Python 3.6+ installed, simply run `python main.py` in Windows or `python3 main.py` in GNU/Linux

## How does it work?

This AI works with pretty much sheer bruteforcing, it generates every possible combination with of the correct LENGTH and starts proceeding by elimination

## What's `testing_main.py`? 

It's a simple script I used to test the solver's speed and average attempts, by default it runs 1000 test runs and gives you detailled results but you can change that.
