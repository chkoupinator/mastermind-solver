import random
import collections
import time

LENGTH = 4


def generate_number(numbers: int = LENGTH)-> str:
    n = list(range(10))
    r = str()
    for i in range(numbers):
        r = f'{r}{n.pop(random.randint(0, len(n)-1))}'
    return r


NUMBER = generate_number()
# print(f"The number to guess is: {NUMBER}")


def check_guess(guess: str, number: str=NUMBER)-> [int, int]:
    number = f'{number}'
    guess = f'{guess}'
    answ = [0, 0]    # 1st pos is amount in right spot, 2nd is amount misplaced 3rd is amount that doesn't exist

    for letter in guess:
        if letter in number:
            if guess.index(letter) == number.index(letter):
                answ[0] += 1
            else:
                answ[1] += 1

    return answ


def generate_all_possible_numbers(length: int=LENGTH):
    possible_digits = list()

    def generate_possible_numbers(number_so_far: str="", digits: [int]=None):
        if len(number_so_far) == length:
            possible_digits.append(number_so_far)
            return
        if digits is None:
            digits = list(range(10))
        for i in range(len(digits)):
            new_digits = list(digits)
            n = new_digits.pop(i)
            generate_possible_numbers(f'{number_so_far}{n}', new_digits)
    generate_possible_numbers()

    return possible_digits


def pop_random(l):
    return l.pop(random.randint(0, len(l) - 1))


def try_to_guess(number: str=NUMBER) -> int:
    possible_digits = generate_all_possible_numbers(LENGTH)
    attempts = 0
    guesses = list()
    results = list()

    while True:
        attempts += 1
        if attempts == 1:
            guess = pop_random(possible_digits)  # type: str
        else:
            guess = None
            while len(possible_digits):
                guess = pop_random(possible_digits)  # type: str

                def is_valid_guess(g):
                    for i in range(attempts - 1):
                        result = check_guess(guesses[i], g)
                        if result != results[i]:
                            return False
                    return True
                if is_valid_guess(guess):
                    break

        guess_result = check_guess(guess, number)
        # print(f"Guess number {attempts}: {guess} gave results {guess_result}")
        guesses.append(guess)
        results.append(guess_result)
        if guess_result[0] == LENGTH:
            if number == guess:
                return attempts
            else:
                print(f"Error in check_guess() returned 4B while number is {number} and guess is {guess}!")
                raise Exception

        if sum(guess_result):
            i = 0
            while i < len(possible_digits):
                num = possible_digits[i]
                in_num = 0
                for index in range(len(guess)):
                    if guess[index] in num:
                        in_num += 1

                if in_num != sum(guess_result):
                    del possible_digits[i]
                else:
                    i += 1
        else:
            i = 0
            while i < len(possible_digits):
                num = possible_digits[i]
                in_num = 0
                for index in range(len(guess)):
                    if guess[index] in num:
                        in_num += 1

                if in_num:
                    del possible_digits[i]
                else:
                    i += 1

        if not len(possible_digits):
            print("Contradiction! No number fits what you gave!")
            raise Exception


attempts = []

for i in range(1000):
    NUMBER = generate_number()
    time.sleep(0.00001)
    attempts.append(try_to_guess())
    print(f"Number N°{i}, which is {NUMBER} guessed in {try_to_guess()} attempts !")
print(f"Average attempts: {sum(attempts)/len(attempts)}, Maximum attempts: {max(attempts)}, "
      f"Minimum attempts: {min(attempts)}\n "
      f"Counter with Number of attempts: Amount of numbers that required that amount of attempts\n"
      f"{collections.Counter(attempts)}")
